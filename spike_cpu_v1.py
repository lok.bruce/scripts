# Here's a modified version of the code that allows you to control the CPU load generated 
# by specifying the number of processes to create and the duration of the load:

import multiprocessing
import os
import time

def cpu_load(duration):
    start_time = time.time()
    while time.time() - start_time < duration:
        pass

if __name__ == '__main__':
    # Get the number of CPUs available
    num_cpus = os.cpu_count()

    # Get user input for number of processes and duration of load
    num_processes = int(input("Enter the number of processes to create: "))
    load_duration = int(input("Enter the duration of the load (in seconds): "))

    # Create a list of processes
    processes = []
    for i in range(num_processes):
        process = multiprocessing.Process(target=cpu_load, args=(load_duration,))
        process.start()
        processes.append(process)

    # Wait for all processes to finish
    for process in processes:
        process.join()
