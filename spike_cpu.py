# This code creates a function cpu_load that runs an infinite loop, which generates CPU load. 
# The main part of the code creates a separate process for each CPU on the server, and each 
# process runs the cpu_load function. This results in high CPU usage on the server.

import multiprocessing
import os

def cpu_load():
    while True:
        pass

if __name__ == '__main__':
    # Get the number of CPUs available
    num_cpus = os.cpu_count()

    # Create a list of processes, one for each CPU
    processes = []
    for i in range(num_cpus):
        process = multiprocessing.Process(target=cpu_load)
        process.start()
        processes.append(process)

    # Wait for all processes to finish
    for process in processes:
        process.join()
